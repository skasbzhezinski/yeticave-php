<?php
/**
 * Возвращеет количество целых часов и остатка минут от настоящего времени до даты
 * @param string $date Дата истечения времени
 * @return array
*/

function get_time_left($date)
{
    date_default_timezone_set("Europe/Moscow");
    $final_date = date_create($date);
    $cur_date = date_create("now");
    $diff = date_diff($cur_date, $final_date);
    $format_diff = $diff->format('%d %H %I');

    $arr = explode(" ", $format_diff);

    $hours = $arr[0] * 24 + $arr[1];
    $minutes = intval($arr[2]);

    $hours = str_pad($hours, 2, "0", STR_PAD_LEFT);
    $minutes = str_pad($minutes, 2, "0", STR_PAD_LEFT);

    $res[] = $hours;
    $res[] = $minutes;
    // print_r($res);

    return $res;
}