<?php
require_once("helpers.php");
require_once("data.php");
require_once("my_functions.php");

$page_content = include_template("../templates/main.php", [
    "categories" => $categories,
    "goods" => $goods
]);
$layout_content = include_template("../templates/layout.php", [
    "content" => $page_content,
    "categories" => $categories,
    "title" => "Главная"
]);

print($layout_content);